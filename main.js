class SuperMath {
  constructor(X, Y, operator) {
    this.X = X;
    this.Y = Y;
    this.operator = operator;
  }

  check() {
    let checkOp;

    if (this.operator === undefined) {
      this.input();
    }

    checkOp = confirm(`Do you want to replace the value ${this.operator}`);

    if (checkOp !== false) {
      this.input();
      this.check();
    }
  }

  input() {
    do {
      this.operator = prompt('Enter the operator + or - or / or * or %', '+');
    } while(
      this.operator !== '+' &&
      this.operator !== '-' &&
      this.operator !== '/' &&
      this.operator !== '*' &&
      this.operator !== '%'
    );

    do {
      this.X = +prompt('Enter the first num', 5);
    } while(isNaN(this.X));

    do {
      this.Y = +prompt('Enter the srcond num', 2);
    } while(isNaN(this.Y));
  }

    operation() {
    switch (this.operator) {
      case '+':
        return this.X + this.Y;
      case '-':
        return this.X - this.Y;
      case '*':
        return this.X * this.Y;
      case '/':
        return this.X / this.Y;
      case '%':
        return this.X * (this.Y / 100);
    }
  }
}

let p = new SuperMath();
p.check(p); // --> no p.input() -> 3 prompt -> рахує
console.log(p.operation());
